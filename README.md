GitSync - synchronization over git
==================================
Author:         Martin Simon
Email:          martiin.siimon@gmail.com
Git:            http://bitbucket.org/martiinsiimon/gitsync
License:        See the LICENSE file
Project info:   GitSync is an easy tool to maintain small files synchronization
                over remote git repository. It's not supposed to synchronize
                big files. To such files use services as DropBox or SpiderOak.
                The main purpose is to synchronize config files among very
                similar systems to keep them sycnhronized and as much same
                as possible.

What's not the purpose:
 * Synchronize big files
